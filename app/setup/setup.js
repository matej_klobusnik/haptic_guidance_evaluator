'use strict';
var SOUNDS_PATH = 'sounds/';
angular.module('hapticPiano.setup', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/setup', {
            templateUrl: 'setup/setup.html',
            controller: 'SetupCtrl'
        });
    }])

    .controller('SetupCtrl', ['$scope', 'DataSource', 'Model', '$filter', 'ArduinoPiano', function ($scope, DS, Model, $filter, ArduinoPiano) {
        $scope.piano = ArduinoPiano;
        $scope.newItem = {};
        $scope.data = DS.data;
        $scope.status = DS.status;
        $scope.addItem = Model.addItem;
        $scope.removeItem = Model.removeItem;

        $scope.changed = function () {
            DS.saveAll();
        };
        $scope.lengthFilter = function (item) {
            return false;
        };


    }]);