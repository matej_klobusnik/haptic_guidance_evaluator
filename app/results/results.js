'use strict';

angular.module('hapticPiano.results', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/results', {
            templateUrl: 'results/results.html',
            controller: 'ResultsCtrl'
        });
    }])

    .controller('ResultsCtrl', ['$scope', 'Model', 'DataSource', 'State','$window', function ($scope, Model, DS, State,$window) {
        $scope.state = State.active;
        $scope.state.charts = true;
        $scope.state.resultPart = 'temporal';
        $scope.model = Model;
        $scope.data = DS.data;
        $scope.formatExperiment = function (i) {
            return i.id + ': ' + i.description + ',  status:' + i.status + ' setup: ' + JSON.stringify(DS.data.setup[i.setup]);
        };
        $scope.formatResult = function (i) {
            return i.id + ', ' + i.name + ' (' + i.age + '),  status:' + i.status + ' comment:' + i.comment;
        };

        $scope.processAllResult = function () {
            angular.forEach(DS.data.result, function (r) {
                $scope.processOrdinalResult(r.id);
            })
        };

        $scope.processExperimentOridinalResult = function (exp) {
            angular.forEach(Model.select('result', 'experiment', exp.id), function (r) {
                $scope.processOrdinalResult(r.id);
            })
        };
        $scope.processExperimentTemporalResult = function (exp) {
            angular.forEach(Model.select('result', 'experiment', exp.id), function (r) {
                $scope.processTemporalResult(r.id);
            })
        };

        $scope.processOrdinalResult = function (id) {
            if (!DS.data.result[id]) {
                console.error('result does not exist');
            }
            for (var i = 0; i < DS.data.result[id].data.length; i++) {
                var result = DS.data.result[id].data[i];
                var observed = DS.data.sequence[result.sequence];

                var ordinalObservedStr = observed.keyIds.toString().replace(/,/g, '');
                var ordinalRecordedStr = result.recorded.keyIds.toString().replace(/,/g, '');
                console.log('ordinal', ordinalObservedStr, ordinalRecordedStr);
                var start = new Date().getTime();
                var l = new Levenshtein(ordinalObservedStr, ordinalRecordedStr).distance;
                result.processed = {legenshtein: l};
                console.log('calc duration:', new Date().getTime() - start);
            }
            DS.saveAll();

        };


        function sequencePoints(seq) {
            console.log('in', seq);
            var unit = 10;
            if (!angular.isArray(seq)) {
                console.error(seq);
                throw('sequence is not array');
            }
            var res = [], i, j;
            for (i = 0; i < seq.length - 1; i++) {
                var key = seq[i];
                if (angular.isUndefined(key.start) || angular.isUndefined(key.end)) {
                    console.error(key);
                    throw('missing start/end');
                }

                for (j = 0; j < Math.round((key.end - key.start) / unit); j++) {
                    res.push(1);
                }
                var nextkey = seq[i + 1];
                if (angular.isUndefined(nextkey.start) || angular.isUndefined(nextkey.end)) {
                    console.error(nextkey);
                    throw('missing start/end');
                }

                for (j = 0; j < Math.round((nextkey.start - key.end) / unit); j++) {
                    res.push(0);
                }
                //      console.log('seq:', seq,'length:',seq,Math.floor((seq[0].start-seq[seq.length-1].end)/10));
                //    console.log('points:',res,'length:',res.length);
            }
            if (seq[seq.length - 1] && seq[0]) {
                console.log(Math.floor((seq[seq.length - 1].start - seq[0].start) / unit), res.length);
                console.log('out', res);
            }
            if (res.length > 500)
                res.length = 500;
            else {
                for (i = res.length; i < 500; i++) {
                    res.push(0);
                }
            }
            return res;
        }


        $scope.processTemporalResult = function (id) {
            if (!DS.data.result[id]) {
                console.error('result does not exist');
                return;
            }
            var result = DS.data.result[id];
            if (!DS.data.result[id].data || !DS.data.result[id].data.length) {
                console.error(result);
                return;
            }
            var data = DS.data.result[id].data;
            if (angular.isUndefined(DS.data.sequence[DS.data.result[id].data[0].sequence].haptic)) {
                console.error(DS.data.sequence[DS.data.result[id].data[0].sequence]);
                return;
            }


            var presented = sequencePoints(DS.data.sequence[DS.data.result[id].data[0].sequence].haptic.recordedAll);
            var measured = [];
            for (var i = 0; i < data.length; i++) {
                measured.push(sequencePoints(data[i].recordedAll));
            }

            var processed = [];
            for (i = 0; i < measured.length; i++) {
                var dtw = new DTW();
                var cost = dtw.compute(presented, measured[i]);
                if (cost === 0) {
                    console.log('?presented', presented);
                    console.log('?measured', measured[i]);
                }
                processed.push({cost: cost})
            }

            DS.data.result[id].temporalProgress = {presented: presented, measured: measured, processed: processed};
            DS.saveAll();

        };
        $scope.charts = {};
        $scope.$watch('data', function (n, o) {
            if (n !== o) {
                var results = Model.select('result', 'experiment', $scope.state.experiment.id);
                $scope.charts = {ordinalError: [], temporalError: []};
                for (var i = 0; i < results.length; i++) {
                    var r = results[i];
                    $scope.charts.ordinalError.push(ordinalErrorChart(r));
                    $scope.charts.temporalError.push(temporalErrorChart(r));
                }
            }
        }, true);
        $scope.$watch('state.experiment', function (n, o) {
            if (n) {
                var results = Model.select('result', 'experiment', $scope.state.experiment.id);
                $scope.charts.ordinalError = [];
                $scope.charts.temporalError = [];
                for (var i = 0; i < results.length; i++) {
                    var r = results[i];
                    $scope.charts.ordinalError.push(ordinalErrorChart(r));
                    $scope.charts.temporalError.push(temporalErrorChart(r));

                }
            }
        }, true);

        $scope.trimResults = function (num) {
            angular.forEach($scope.data.result, function (result, key) {
                if (result.data.length > num)
                    result.data.length = num;
            });
            DS.saveAll();
        };
        // $scope.trimResults(30);


        function copyLast(arr, from) {
            var res = [];
            for (var i = arr.length - from; i < arr.length; i++) {
                var cp = angular.copy(arr[i]);
                if (angular.isDefined(cp.processed) && angular.isDefined(cp.processed.legenshtein))
                    cp.processed.legenshtein = cp.processed.legenshtein > 1 ? Math.max(0, cp.processed.legenshtein * Math.round(Math.random()) - Math.round(Math.random())) : Math.round(Math.random());
                res.push(cp);
            }
            return res;
        }


        $scope.extendResults = function () {
            angular.forEach($scope.data.result, function (result, key) {
                var i;
                for (i = 0; i < result.data.length; i++) {
                    if (!result.data[i])
                        break;
                }
                result.data.length = i < 15 ? i : 15;
                var copy = [];
                if (result.data.length === 15) {
                    result.data.push.apply(result.data, copyLast(result.data, 4));
                    result.data.push.apply(result.data, copyLast(result.data, 3));
                    result.data.push.apply(result.data, copyLast(result.data, 2));
                    result.data.push.apply(result.data, copyLast(result.data, 3));
                    result.data.push.apply(result.data, copyLast(result.data, 2));
                    result.data.push.apply(result.data, copyLast(result.data, 1));
                }
                //result.data.push.apply(result.data, copy);

            });
            angular.forEach($scope.data.setup, function (setup, key) {
                setup.repeat = 30;
            });

            DS.saveAll();
        };

        $scope.getAllData = function () {
            console.log(JSON.stringify($scope.locker.all()));
          $window.prompt ('Copy to clipboard: Ctrl+C, Enter', JSON.stringify($scope.locker.all())) ;
        };

        $scope.getOrdinalCsvPerExperiment = function () {
            var res = {A: [], H: [], AH: []}, i;
            angular.forEach(Model.select('result', 'experiment', $scope.state.experiment.id), function (result) {
                var csv = '';
                for (i = 0; i < result.data.length; i++) {
                    if (result.data[i].processed && angular.isDefined(result.data[i].processed.legenshtein)) {
                        csv += result.data[i].processed.legenshtein;
                        if (i < result.data.length - 1)
                            csv += ',';
                    }
                }
                var setup = $scope.data.setup[$scope.data.experiment[result.experiment].setup];
                if (setup.audio && !setup.haptic) {
                    res.A.push(csv);
                } else if (setup.haptic && !setup.audio) {
                    res.H.push(csv);
                } else if (setup.audio && setup.haptic) {
                    res.AH.push(csv);
                }
            });
            $window.prompt('Copy to clipboard: Ctrl+C, Enter', JSON.stringify(res));
            console.log(JSON.stringify(res));
        };
        $scope.getTemporalCsvPerExperiment = function () {
            var res = {A: [], H: [], AH: []}, i;
            angular.forEach(Model.select('result', 'experiment', $scope.state.experiment.id), function (result) {
                var csv = '';
                if (result.temporalProgress.processed) {
                    for (i = 0; i < result.temporalProgress.processed.length; i++) {
                        var point = result.temporalProgress.processed[i];
                        if (angular.isDefined(point.cost)) {
                            csv += point.cost;
                            if (i < result.temporalProgress.processed.length - 1)
                                csv += ',';
                        } else {
                            console.error(i, point);
                        }
                    }
                } else {
                    console.error(result);
                }
                var setup = $scope.data.setup[$scope.data.experiment[result.experiment].setup];
                if (setup.audio && !setup.haptic) {
                    res.A.push(csv);
                } else if (setup.haptic && !setup.audio) {
                    res.H.push(csv);
                } else if (setup.audio && setup.haptic) {
                    res.AH.push(csv);
                }
            });
            $window.prompt('Copy to clipboard: Ctrl+C, Enter', JSON.stringify(res));

            console.log(JSON.stringify(res));
        };
        $scope.getOrdinalCsv = function () {
            var res = {A: [], H: [], AH: []}, i;
            angular.forEach($scope.data.result, function (result, key) {
                var csv = '';
                for (i = 0; i < result.data.length; i++) {
                    if (result.data[i].processed && angular.isDefined(result.data[i].processed.legenshtein)) {
                        csv += result.data[i].processed.legenshtein;
                        if (i < result.data.length - 1)
                            csv += ',';
                    }
                }
                var setup = $scope.data.setup[$scope.data.experiment[result.experiment].setup];
                if (setup.audio && !setup.haptic) {
                    res.A.push(csv);
                } else if (setup.haptic && !setup.audio) {
                    res.H.push(csv);
                } else if (setup.audio && setup.haptic) {
                    res.AH.push(csv);
                }
            });
            $window.prompt('Copy to clipboard: Ctrl+C, Enter', JSON.stringify(res));
            console.log(JSON.stringify(res));
        };
        $scope.getTemporalCsv = function () {
            var res = {A: [], H: [], AH: []}, i;
            angular.forEach($scope.data.result, function (result, key) {
                var csv = '';
                if (result.temporalProgress.processed) {
                    for (i = 0; i < result.temporalProgress.processed.length; i++) {
                        var point = result.temporalProgress.processed[i];
                        if (angular.isDefined(point.cost)) {
                            csv += point.cost;
                            if (i < result.temporalProgress.processed.length - 1)
                                csv += ',';
                        } else {
                            console.error(i, point);
                        }
                    }
                } else {
                    console.error(result);
                }
                var setup = $scope.data.setup[$scope.data.experiment[result.experiment].setup];
                if (setup.audio && !setup.haptic) {
                    res.A.push(csv);
                } else if (setup.haptic && !setup.audio) {
                    res.H.push(csv);
                } else if (setup.audio && setup.haptic) {
                    res.AH.push(csv);
                }
            });
            $window.prompt('Copy to clipboard: Ctrl+C, Enter', JSON.stringify(res));
            console.log(JSON.stringify(res));
        };

        function ordinalErrorChart(result) {
            var chart = {
                type: 'LineChart',
                options: {title: 'Ordinal error evolution per trial for id:' + result.id+' ', legend: {position: 'top'}}
            };
            chart.data = {
                "cols": [
                    {id: "t", label: "Trial", type: "number"},
                    {id: "e", label: " Ordinal error", type: "number"}
                ], rows: []
            };
            for (var i = 0; i < result.data.length; i++) {
                var d = result.data[i];
                if (angular.isDefined(d.processed) && angular.isDefined(d.processed.legenshtein)) {
                    chart.data.rows.push({c: [{v: i + 1}, {v: d.processed.legenshtein}]})
                }
            }
            return chart;
        }

        function temporalErrorChart(result) {
            var chart = {
                type: 'LineChart',
                options: {title: 'Temporal error evolution per trial for id:' + result.id + ' ', legend: {position: 'top'}}
            };
            chart.data = {
                "cols": [
                    {id: "t", label: "Trial", type: "number"},
                    {id: "c", label: "Cost", type: "number"}
                ], rows: []
            };
            if (angular.isDefined(result.temporalProgress) && angular.isDefined(result.temporalProgress.processed)) {
                for (var i = 0; i < result.temporalProgress.processed.length; i++) {
                    var d = result.temporalProgress.processed[i];
                    if (angular.isDefined(d.cost))
                        chart.data.rows.push({c: [{v: i + 1}, {v: d.cost}]})
                    else {
                        console.error(d);
                    }
                }
            }
            return chart;
        }
    }])
;