'use strict';

angular.module('hapticPiano.version', [
  'hapticPiano.version.interpolate-filter',
  'hapticPiano.version.version-directive'
])

.value('version', '0.1');
