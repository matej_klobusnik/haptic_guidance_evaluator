'use strict';
angular.module('hapticPiano.experiment', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/experiment', {
            templateUrl: 'experiment/experiment.html',
            controller: 'ExperimentCtrl'
        });
    }])
    .controller('ExperimentCtrl', ['$scope', '$timeout', '$interval', 'ArduinoPiano', 'DataSource', 'locker', 'State', 'Model', function ($scope, $timeout, $interval, ArduinoPiano, DS, locker, State, Model) {
        $scope.state = State.active;
        $scope.state.testing = {status: 'stopped'};
        if (angular.isUndefined($scope.active.recordings))
            $scope.active.recordings = [];
        $scope.newItem = {};
        $scope.piano = ArduinoPiano;
        $scope.data = DS.data;
        $scope.model = Model;
        $scope.input = {sequence: localStorage.getItem('input.sequence') ? localStorage.getItem('input.sequence') : ''};

        $scope.formatResult = function (i) {
            return i.id + ': ' + i.name + ' (' + i.age + '),  status:' + i.status + ' comment:' + i.comment;
        };
        $scope.formatExperiment = function (i) {
            return i.id + ': ' + i.description + ',  status:' + i.status + ' setup: ' + JSON.stringify(DS.data.setup[i.setup]);
        };

        $scope.clearResult = function (result) {
            if (angular.isDefined(result) && angular.isDefined(result.data)) {
                result.data = [];
                result.status = 'new';
                DS.saveAll();
            }
        };
        $scope.deleteResult = function (result) {
            if (angular.isDefined(result) && angular.isDefined(DS.data.result[result.id])) {
                delete DS.data.result[result.id];
                DS.saveAll();

            }
        };
        $scope.startTesting = function () {
            var setup = $scope.setup();
            if (setup.haptic && !setup.audio) {
                $scope.noise = $scope.piano.audio.load('sounds/noise.mp3');
                $scope.noise.volume = 0.4;
                $scope.noise.play();
            }

            if (!$scope.state.experiment) {
                console.error(state);
                return;
            }
            $scope.state.testing.status = 'running';
            $scope.state.testing.stage = 'overview';
            $scope.countoff = 20;
            $interval(function () {
                $scope.countoff--;
            }, 1000, 20);
            $timeout(function () {
                nextTrial();
            }, 21000);
        };
        function nextTrial() {
            if ($scope.state.testing.status === 'stopped') {
                console.log('stopped');
                return;
            }
            var setup = $scope.setup();
            var result = $scope.data.result[$scope.state.result.id];
            var seqId = $scope.data.sequencePool[setup.sequencePool].sequence[result.data.length % $scope.data.sequencePool[setup.sequencePool].sequence.length];
            var sequence = $scope.data.sequence[seqId];
            $scope.state.testing.stage = 'ocountoff';
            $scope.state.testing.status = 'running';

            $timeout(function () {
                $scope.state.testing.stage = 'observation';
                if (setup.audio && setup.haptic) {
                    $scope.piano.playOneNoteSequence(sequence.keyIds, sequence.durations);
                } else if (setup.audio) {
                    $scope.piano.playAudioSequence(sequence.id);
                } else if (setup.haptic) {
                    $scope.piano.playOneNoteSequence(sequence.keyIds, sequence.durations);
                } else {
                    console.error('wrong setup', setup);
                }

            }, setup.countoff * 1000);

            //$timeout(function () {
            //    $scope.state.testing.stage = 'pcountoff';
            //}, 1000 * (setup.trainDur + setup.countoff));

            $timeout(function () {
                $scope.state.testing.stage = 'playback';
            }, (setup.trainDur + setup.countoff) * 1000);

            $timeout(function () {
                var trialRecord = angular.extend({sequence: sequence.id}, $scope.piano.sliceKeyHistory(setup.playDur * 1000, 0));
                result.data.push(trialRecord);
                DS.saveAll();
            }, (setup.playDur + setup.trainDur + 2 * setup.countoff) * 1000);

            if ($scope.state.result.data.length - 1 < setup.repeat) {

                $timeout(function () {
                    nextTrial();
                }, (setup.playDur + setup.trainDur + 2 * setup.countoff) * 1000);
            } else {
                if ($scope.noise) {
                    $scope.noise.stop();
                    delete $scope.noise;
                }
                $scope.state.result.status = 'finished';
                $scope.state.testing.status = 'stopped';
            }
        }


        $scope.setup = function () {
            return $scope.data.setup[$scope.state.experiment.setup];
        };


        $scope.$watch('state', function (n, o) {
            console.log('state', n);
        }, true);


    }])
;