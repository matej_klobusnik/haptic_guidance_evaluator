'use strict';
// Declare app level module which depends on views, and components
angular.module('hapticPiano', ['ngRoute', 'hapticPiano.devel', 'hapticPiano.setup', 'hapticPiano.experiment', 'hapticPiano.results', 'hapticPiano.version', 'angular-locker', 'ngAudio', 'googlechart'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/devel'});
        $routeProvider.when('/about', {
            templateUrl: 'about.html',
            controller: 'AboutCtrl'
        });
    }])
    .controller('AboutCtrl', ['$scope', function ($scope) {

    }])
    .controller('MainController', ['$scope', '$filter', 'DataSource', 'ArduinoPiano', 'State', function ($scope, $filter, DS, AruduinoPiano, State) {
        $scope.active = State.active;
        $scope.active.globalFolder=3;
        $scope.piano = AruduinoPiano;
        $scope.locker = DS.locker;
        $scope.data = DS.data;
        $scope.status = DS.status;
        $scope.hideMenu = false;
        $scope.saveAll = DS.saveAll;
        $scope.actionPos = 0;
        $scope.actionInfo = function () {
            return '' + $scope.status.actions[$scope.status.actions.length - 1 - $scope.actionPos] + '   : ' + $scope.actionPos;
        };
        $scope.localStorageInfo = function () {
            return 'Storage: ' + $filter('number')(JSON.stringify($scope.locker.all()).length / 5100000, 1) + ' %';
        };
        $scope.localStorageInfoTitle = function () {
            return 'Storage char cnt: ' + JSON.stringify($scope.locker.all()).length;
        };
    }])
    .directive('autoActive', ['$location', function ($location) {
        return {
            restrict: 'A',
            scope: false,
            link: function (scope, element) {
                function setActive() {
                    var path = $location.path();
                    if (path) {
                        angular.forEach(element.find('li'), function (li) {
                            var anchor = li.querySelector('a');
                            if (anchor.href.match('#' + path + '(?=\\?|$)')) {
                                angular.element(li).addClass('active');
                            } else {
                                angular.element(li).removeClass('active');
                            }
                        });
                    }
                }

                setActive();

                scope.$on('$locationChangeSuccess', setActive);
            }
        }
    }])
    .factory('ArduinoPiano', ['$timeout', 'Utils', '$rootScope', 'locker', 'DataSource', 'ngAudio','State', function ($timeout, U, $rootScope, locker, DS, audio, State) {
        var state = State.active;
        BO.enableDebugging = true;
        var host = 'localhost', piano, i,
            keyDefs = [
                {
                    id: 5,
                    note: 'g',
                    servoPin: 10,
                    delay: 110,
                    offAngle: 85,
                    sensorPin: 1,
                    triggerPoint: 0.5
                },
                {
                    id: 4,
                    note: 'f',
                    servoPin: 9,
                    offAngle: 80,
                    delay: 110,
                    sensorPin: 4,
                    triggerPoint: 0.4
                },
                {
                    id: 3,
                    note: 'e',
                    servoPin: 6,
                    offAngle: 80,
                    delay: 110,
                    sensorPin: 2,
                    triggerPoint: 0.4
                },
                {
                    id: 2,
                    note: 'd',
                    servoPin: 5,
                    offAngle: 70,
                    delay: 110,
                    sensorPin: 3,
                    triggerPoint: 0.4
                },
                {
                    id: 1,
                    note: 'c',
                    servoPin: 3,
                    offAngle: 85,
                    delay: 110,
                    sensorPin: 0,
                    triggerPoint: 0.5
                }];

        var pianoExtend = {keyPressed: false, keyHist: [], keys: {}};
        piano = new BO.IOBoard(host, 8887);
        angular.extend(piano, pianoExtend);
        piano.audio = audio;
        piano.onReady = function () {
            $timeout(function () {
                piano.removeEventListener(BO.IOBoardEvent.READY, piano.onReady);
                for (i = 0; i < keyDefs.length; i++) {
                    piano.keys[keyDefs[i].id] = new Key(keyDefs[i], piano);
                }
                //   piano.calibrateDelays();
            });
        };
        piano.playAudioSequence = function (id, volume) {
            var audio = piano.audio.load('sounds/'+state.globalFolder+'/' + id + '.mp3');
            if (angular.isDefined(volume)) {
                audio.volume = volume;

            } else
                audio.volume = 1;
            audio.play();
        };
        piano.addEventListener(BO.IOBoardEvent.READY, piano.onReady);
        function Key(keyDef, board) {
            var self = this;
            angular.extend(this, keyDef);
            this.board = board;
            this.servo = new BO.io.Servo(board, board.getDigitalPin(this.servoPin));
            board.enableAnalogPin(this.sensorPin);
            this.sensor = board.getAnalogPin(this.sensorPin);
            this.sensor.addFilter(new BO.filters.TriggerPoint([this.triggerPoint, 0.05]));
            this.sensor.addEventListener(BO.PinEvent.FALLING_EDGE, function (e) {
                console.log('key start', self.id, new Date().toISOString(), new Date().getTime());
                self.start = new Date().getTime();
                $rootScope.$digest();
            });
            this.sensor.addEventListener(BO.PinEvent.RISING_EDGE, function (e) {
                if (!self.start) {
                    console.log('no start - skipping key end', self, new Date());
                    return;
                }
                console.log('key end', self.id, new Date().toISOString(), new Date().getTime());
                self.end = new Date().getTime();

                board.keyHist.push({
                    keyId: self.id,
                    start: self.start,
                    end: self.end,
                    pressDuration: self.end - self.start
                });
                delete self.start;
                delete self.end;
                $rootScope.$digest();
            });
            this.delay = 120;
            this.minDuration = 250;
            this.onAngle = this.offAngle - 35;
            this.servo.angle = this.offAngle;
            this.on = function () {
                self.servo.angle = self.onAngle;
                self.servo.on = true;
            };
            this.off = function () {
                self.servo.angle = self.offAngle;
                self.servo.on = false;
            };

            this.toogle = function () {
                if (self.servo.on)
                    self.off();
                else
                    self.on();
            };

            this.play = function (duration, offset, lastTone) {
                duration = self.minDuration ? Math.max(duration, self.minDuration) : duration;
                console.log('scheduling play on ', offset - self.delay);
                console.log('scheduling play off ', offset + duration - 2 * self.delay);
                $timeout(function () {
                    self.on();
                }, offset - self.delay);
                $timeout(function () {
                    self.off();
                    if (lastTone)
                        board.sequenceInProgress = false;
                }, offset + duration - 2 * self.delay);
                return offset + duration;
            };

        }


        piano.startRecording = function () {
            piano.recording = {};
            var start = new Date().getTime();
            if (angular.isUndefined(piano.recording.start))
                piano.recording.start = start;
            return start;
        };
        piano.stopRecording = function () {
            var stop = new Date().getTime(), i;
            if (piano.isRecording()) {
                for (i = 0; i < piano.keyHist.length; i++) {
                    if (piano.keyHist[i].start >= piano.recording.start) {
                        var startPos = i;
                        break;
                    }
                }
                if (angular.isUndefined(startPos)) {
                    delete piano.recording.start;
                    return [];
                }

                for (i = startPos; i < piano.keyHist.length; i++) {
                    if (piano.keyHist[i].start >= stop) {
                        var stopPos = i;
                        break;
                    }
                }
                if (angular.isUndefined(stopPos))
                    stopPos = piano.keyHist.length;

                var res = [];
                for (i = startPos; i < stopPos; i++) {
                    res.push(piano.keyHist[i]);
                }
                delete piano.recording.start;
                return res;
            } else {
                console.error('not recording,nothing to stop');
            }

        };
        piano.seqDuration = function (durations) {
            var res = 0;
            for (var i = 0; i < durations.length; i++) {
                res += durations[i];
            }
            return res;
        };


        piano.sliceKeyHistory = function (duration, offset) {
            var res = [], keyIds = [], durations = [], pressDurations = [];

            var stop = new Date().getTime() - offset, i;
            var start = stop - duration - offset;

            for (i = 0; i < piano.keyHist.length; i++) {
                if (piano.keyHist[i].start >= start) {
                    var startPos = i;
                    break;
                }
            }
            if (angular.isUndefined(startPos)) {
                return {
                    recorded: {keyIds: keyIds, durations: durations, pressDurations: pressDurations},
                    recordedAll: res
                };
            }

            for (i = startPos; i < piano.keyHist.length; i++) {
                if (piano.keyHist[i].start >= stop) {
                    var stopPos = i;
                    break;
                }
            }
            if (angular.isUndefined(stopPos))
                stopPos = piano.keyHist.length;

            for (i = startPos; i < stopPos; i++) {
                res.push(piano.keyHist[i]);
            }

            for (var j = 0; j < res.length; j++) {
                var r = res[j];
                keyIds.push(r.keyId);
                durations.push(j < res.length - 1 ? res[j + 1].start - res[j].start : null);
                pressDurations.push(r.pressDuration);
            }

            console.log('recorded keyIds', keyIds);
            console.log('recorded durations', durations);
            console.log('recorded press durations', pressDurations);
            console.log('recorded all', res);
            return {recorded: {keyIds: keyIds, durations: durations, pressDurations: pressDurations}, recordedAll: res};
        };


        piano.isRecording = function () {
            return piano.recording && piano.recording.start;
        };
        piano.recordPeriod = function (offset, duration, storage) {
            if (piano.isRecording()) {
                console.error('already recording');
                return;
            }
            $timeout(function () {
                piano.startRecording();
                console.log('start recording', new Date().toISOString());
            }, offset);
            $timeout(function () {
                console.log('stop recording', new Date().toISOString());

                var result = piano.stopRecording(), keyIds = [], durations = [], pressDurations = [];

                for (var j = 0; j < result.length; j++) {
                    var r = result[j];
                    keyIds.push(r.keyId);
                    durations.push(r.duration);
                    pressDurations.push(r.pressDuration);
                }
                piano.lastRecord = result;
                piano.lastRecordSeparated = {keyIds: keyIds, durations: durations, pressDurations: pressDurations};
                storage.recorded = {keyIds: keyIds, durations: durations, pressDurations: pressDurations};
                storage.recordedAll = result;
                console.log('recorded keyIds', keyIds);
                console.log('recorded durations', durations);
                console.log('recorded press durations', pressDurations);
                console.log('recorded all', result);
            }, offset + duration);
        };


        piano.playOneNoteSequence = function () {
            if (!piano.isReady || angular.isUndefined(piano.keys))
                throw('piano not ready');
            var sequence = U.parseSequence.apply(this, arguments);
            if (sequence.keyIds.length !== sequence.durations.length) {
                console.error('playOneNote -  wrong sequence:', sequence);
                throw('sequence.keyIds.length!=== sequence.durations.length')
            }
            var startOffset = 500, offset;
            offset = startOffset;
            piano.sequenceInProgress = true;
            for (i = 0; i < sequence.keyIds.length; i++) {
                var keyId = sequence.keyIds[i];
                var duration = sequence.durations[i];
                if (angular.isDefined(piano.keys[keyId])) {
                    piano.keys[keyId].play(duration, offset, i === sequence.keyIds.length - 1)
                }
                offset += duration;
            }
            var result = {played: sequence};
            piano.recordPeriod(startOffset - 100, offset + 100, result);
            return result;
        };

        piano.recordHaptic = function (seqId, keyIds, durations) {
            var totalDuration = piano.seqDuration(durations);
            piano.playOneNoteSequence(keyIds, durations);
            $timeout(function () {
                var recorded = piano.sliceKeyHistory(totalDuration + 1000, 0);
                console.log('recorded',recorded);
                DS.data.sequence[seqId].haptic = recorded;
                DS.saveAll();
            }, totalDuration + 1000);
        };


        piano.playOneNoteExact = function (seq) {
            var tones = parseStringSequence(seq);
            angular.forEach(tones, function (tone, i) {
                $timeout(function () {
                    tone.key.on();
                }, 0);
                $timeout(function () {
                    tone.key.off();
                }, tone.duration);
            });
        };

        piano.playAll = function (duration) {
            angular.forEach(piano.keys, function (key, id) {
                $timeout(function () {
                    key.on();
                });
                $timeout(function () {
                    key.off();
                }, duration);
            });
        };
        piano.playChordsDemo1 = function () {
            piano.sequenceInProgress = true;
            var offset = 500;
            piano.keys[1].play(250, offset);
            piano.keys[3].play(250, offset);
            piano.keys[2].play(250, offset + 250);
            piano.keys[4].play(250, offset + 250);
            piano.keys[3].play(500, offset + 500);
            piano.keys[5].play(500, offset + 500);
            piano.keys[2].play(500, offset + 1000);
            piano.keys[4].play(500, offset + 1000);
            piano.keys[1].play(500, offset + 1500);
            piano.keys[3].play(500, offset + 1500, true);
        };


        piano.isKeyPressed = function () {
            for (var k in piano.keys) {
                if (piano.keys.hasOwnProperty(k) && piano.keys[k].isPressed()) {
                    return true;
                }
            }
        };

        piano.calibrateDegrees = function () {
            for (var k in piano.keys) {
                if (piano.keys.hasOwnProperty(k)) {
                    var key = piano.keys[k];
                    $timeout(function (key) {
                        while (!isSoundPlaying()) {
                            key.servo.angle = key.servo.angle + 1;
                        }
                        key.onAngle = key.servo.angle;
                    }, 300 * i, true, key);
                }

            }
        };

        piano.calibrateDelays = function () {
            angular.forEach(piano.keys, function (key) {
                $timeout(function () {
                    piano.calibrateDelay(key);
                }, key.id * 500);
            });
        };

        piano.calibrateDelay = function (key) {
            key.on();
            key.startCalib = new Date().getTime();
            var calibHandler = function () {
                key.sensor.removeEventListener(BO.PinEvent.FALLING_EDGE, calibHandler);
                key.off();
                if (key.startCalib) {
                    key.delay = new Date().getTime() - key.startCalib;
                    console.log('calibrating delay', key.id, key.delay);
                    delete key.startCalib;
                }
            };
            key.sensor.addEventListener(BO.PinEvent.FALLING_EDGE, calibHandler);
        };

        function calibOnPress(e) {
            //key.off();

        }

        function sequencePlanner() {

        }


        return piano;
    }]).factory('Utils', ['$timeout', function ($timeout) {
        return {
            findInArray: function (arr, name, value, returnIndex) {
                if (!angular.isArray(arr))
                    return;
                for (var i = 0; i < arr.length; i++) {
                    if (angular.isObject(arr[i]) && angular.isDefined(arr[i][name]) && angular.equals(arr[i][name], value)) {
                        return returnIndex ? i : arr[i];
                    }
                }

            },
            findInObject: function (object, name, value, returnPropName) {
                if (!angular.isObject(object))
                    return;
                for (var p in object) {
                    if (object.hasOwnProperty(p) && angular.equals(object[p][name], value)) {
                        return returnPropName ? p : object[p];
                    }
                }
            },
            parseSequence: parseSequence,
            parseToneArray: parseToneArray,
            parseStringSequence: parseStringSequence
        };

        function parseStringSequence(seq) {
            var tones = seq.split(/\s+/), keyIds = [], durations = [];
            for (var i = 0; i < tones.length; i++) {
                if (tones[i].trim().length > 0) {
                    var tone = tones[i].split(',');
                    if (tone.length !== 2 && !isNaN(tone[1].trim()) && !isNaN(tone[0].trim())) {
                        console.error('sequence', seq);
                        console.error('tone', tone);
                        throw('parse sequence -wrong format');
                    }
                    keyIds.push(parseInt(tone[0]));
                    durations.push(parseInt(tone[1]));
                }
            }
            return {keyIds: keyIds, durations: durations};
        }

        function parseToneArray(keys) {
            if (!angular.isObject(keys) && !angular.isArray(keys)) {
                console.error('parseKeyAray - wrong input', keys);
                throw('parseKeyAray - wrong input');
            }
            var keyIds = [], durations = [];
            angular.forEach(keys, function (item, id) {
                if (!(item.dur || item.duration) || !item.keyId) {
                    console.error('parseKeyAray - missing keyId or dur', keys, item);
                    throw('parseKeyAray - missing keyId or dur');
                }
                keyIds.push(item.keyId);
                durations.push(item.dur ? item.dur : item.duration);
            });
            return {keyIds: keyIds, durations: durations};
        }

        function parseSequence() {
            var a = arguments;
            if (a.length === 1) {
                if (angular.isString(a[0])) {
                    return parseStringSequence(a[0]);
                } else if (angular.isArray(a[0]) || angular.isObject(a[0])) {
                    return parseToneArray(a[0]);
                }
            } else if (a.length === 2) {
                if ((angular.isArray(a[0]) || angular.isObject(a[0])) && (angular.isArray(a[1]) || angular.isObject(a[1])) && a[0].length === a[1].length && a[0][0] && a[1][0]) {
                    return {keyIds: a[0], durations: a[1]};
                }
            }
            console.error(' wrong args', a);
            throw('parse sequene incorect args');
        }


    }]).directive('keyboard', function () {
        return {
            scope: {
                keys: '=keys'
            },
            link: link
        };

        function link(scope, element, attrs) {
            var mapper = {186: 5, 76: 4, 75: 3, 74: 2, 77: 1};

            element.bind('keydown', function (event) {
                if (scope.keys[mapper[event.which]]) {
                    scope.keys[mapper[event.which]].on();
                }
                element.val('');
                event.stopImmediatePropagation();
            });
            element.bind('keypress', function (event) {
                element.val('');
                event.stopImmediatePropagation();
            });
            element.bind('keyup', function (event) {
                if (scope.keys[mapper[event.which]]) {
                    scope.keys[mapper[event.which]].off();
                }
                element.val('');
                event.stopImmediatePropagation();
            });
        }
    }).directive('notePicker', function () {
        return {
            scope: {},
            link: function (scope, element, attrs) {

            },
            template: ''
        }
    }).factory('Model', ['locker', 'DataSource', 'Utils', function (locker, DS, Utils) {
        var definitions = {
            experiment: {usages: ['result'], one: ['setup']},
            setup: {usages: ['experiment'], one: ['sequencePool']},
            sequencePool: {usages: ['setup'], many: ['sequence']},
            sequence: {usages: ['sequencePool']},
            result: {usages: [], one: ['experiment']}
        };//just plugin code completion
        var Model = {
            experiment: function (item) {
                this.date = new Date().toISOString();
                angular.extend(this, processInitItem(item, 'experiment'));
                this.name = item.name ? item.name : name;
            }, result: function (item) {
                angular.extend(this, processInitItem(item, 'result'));
                this.data = [];
                this.addTrialResult = function (sequenceId, playedSequence) {
                    this.data.push({sequenceId: sequenceId, playedSequence: new Model.sequence(playedSequence)});
                }
            }, setup: function (item) {
                angular.extend(this, processInitItem(item, 'setup'));
            }, sequencePool: function (item) {
                angular.extend(this, processInitItem(item, 'sequencePool'));
            }, sequence: function () {
                this.id = nextId('sequence');
                var sq = Utils.parseSequence.apply(this, arguments);
                angular.extend(this, sq);
            }
        };

        return {
            addItem: addItem,
            removeItem: removeItem,
            usages: usages,
            processAllFk: processInitItem,
            validateFk: validateFk,
            processFk: processFk,
            select: function (from, prop, id) {
                var res = [];
                angular.forEach(DS.data[from], function (item) {
                    if (parseInt(item[prop]) === id)
                        res.push(item);
                });
                return res;
            }
        };

        function nextId(propName) {
            var i = locker.get(propName);
            var maxId = 0;
            angular.forEach(i, function (p) {
                if (angular.isDefined(p.id) && parseInt(p.id) > maxId)
                    maxId = parseInt(p.id);
            });
            return maxId + 1;
        }


        function addItem(modelName, item) {
            try {
                var newItem = new Model[modelName](item);
                DS.data[modelName][newItem.id] = newItem;
                console.log('Add ' + modelName + ' - OK', item);
                DS.status.actions.push('Add ' + modelName + ' - OK');
                DS.status.changed = true;
                DS.save(modelName);
            } catch (err) {
                console.log('Add ' + modelName + ' - ' + err, item);
                DS.status.actions.push('Add ' + modelName + ' - ' + err);
            }
            DS.saveAll();
        }

        function removeItem(modelName, id) {
            var used = usages(modelName, DS.data[modelName][id]);
            if (angular.isObject(used)) {
                console.log('Remove ' + modelName + ' id:' + id + ' - Failed. Used by: ' + JSON.stringify(used));
                DS.status.actions.push('Remove ' + modelName + ' id:' + id + ' - Failed. Used by: ' + JSON.stringify(used));
            } else {
                delete DS.data[modelName][id];
                DS.status.actions.push('Remove ' + modelName + ' id:' + id + ' - Ok');
                DS.status.changed = true;
            }
        }

        function parseIds(ids) {
            var res = [];
            if (angular.isArray(ids)) {
                for (var j = 0; j < ids.length; j++) {
                    var id = ids[j];
                    if (!isNaN(id))
                        res.push(parseInt(id));
                }
            } else if (angular.isString(ids)) {
                var splitted = ids.split(/,| /);
                for (var i = 0; i < splitted.length; i++) {
                    var idStr = splitted[i];
                    res.push(parseInt(idStr));
                }
            }
            return res;
        }

        function processInitItem(item, modelName) {
            var res = {id: nextId(modelName)}, fkName, i;

            if (definitions[modelName].many) {
                for (i = 0; i < definitions[modelName].many.length; i++) {
                    fkName = definitions[modelName].many[i];
                    if (item[fkName]) {
                        res[fkName] = parseIds(item[fkName]);
                        for (var j = 0; j < res[fkName].length; j++) {
                            var id = res[fkName][j];
                            if (!angular.isObject(DS.data[fkName][id])) {
                                res[fkName].splice(j, 1);
                                DS.status.actions.push(fkName + ' - invalid id:' + id);
                                j--;
                            }
                        }
                    }
                }
            }
            if (definitions[modelName].one) {
                for (i = 0; i < definitions[modelName].one.length; i++) {
                    fkName = definitions[modelName].one[i];
                    if (item[fkName]) {
                        res[fkName] = parseInt(item[fkName]);
                        if (!isNaN(res[fkName]) && !angular.isObject(DS.data[fkName])) {
                            DS.status.actions.push(fkName + ' - invalid id:' + id);
                            res[fkName] = null;
                        }
                    }
                }
            }
            return angular.extend({}, item, res);
        }


        function processFk(value, fkName, many) {
            if (many) {
                var ids = parseIds(value);
                var res = [];
                for (var j = 0; j < ids.length; j++) {
                    if (angular.isObject(DS.data[fkName][ids[j]])) {
                        res.push(ids[j]);
                    } else
                        DS.status.actions.push(fkName + ' - invalid id:' + ids[j]);
                }
                return res;
            } else {
                var id = parseInt(value);
                if (angular.isObject(DS.data[fkName]))
                    return id;
                else
                    DS.status.actions.push(fkName + ' - invalid id:' + id);
            }
        }

        function validateFk(item, fkIdStr, fkName) {
            var res = [], i;
            if (angular.isArray(item[fkName])) {
                res = parseIds(fkIdStr);
                for (var j = 0; j < res.length; j++) {
                    var id = res[j];
                    if (!angular.isObject(DS.data[fkName][id])) {
                        res[j].splice(j, 1);
                        j--;
                        DS.status.actions.push(fkName + ' - invalid id:' + id);
                    }
                }
            } else if (angular.isNumber(item[fkName])) {
                res = parseInt(fkIdStr);
                if (!isNaN(res[fkName]) && !angular.isObject(DS.data[fkName])) {
                    DS.status.actions.push(fkName + ' - invalid id:' + id);
                    return false;
                }
            }

            return res;
        }

        function usages(modelName, item) {
            var res = {};
            if (!item.id || !definitions[modelName].usages.length)
                return;
            for (var i = 0; i < definitions[modelName].usages.length; i++) {
                var depModelName = definitions[modelName].usages[i];
                if (angular.isObject(DS.data[depModelName])) {
                    angular.forEach(DS.data[depModelName], function (dependentItem) {
                        if (angular.isArray(dependentItem[modelName])) {
                            for (var j = 0; j < dependentItem[modelName].length; j++) {
                                var arrItem = dependentItem[modelName][j];
                                if (parseInt(arrItem) === item.id) {
                                    if (angular.isUndefined(res[depModelName])) {
                                        res[depModelName] = [];
                                        res[depModelName].push(item.id);
                                    }
                                    else
                                        res[depModelName].push(item.id);
                                }
                            }

                        } else if (parseInt(dependentItem[modelName]) === item.id) {
                            if (angular.isUndefined(res[depModelName])) {
                                res[depModelName] = [];
                                res[depModelName].push(item.id);
                            }
                            else
                                res[depModelName].push(item.id);
                        }
                    });
                }
            }
            if (Object.keys(res).length === 0)
                return;
            return res;
        }

    }]).factory('DataSource', ['$window', '$document', 'locker', function ($window, $document, locker) {
        var HP_PREVIOUS = 'hapticPiano_previous', MAX_LOCAL_CAPACITY = 5100000, data = {};
        var modelNames = ['experiment', 'setup', 'sequencePool', 'sequence', 'result'];

        var status = {
            addAction: function (str) {
                status.actions.push(str);
            },
            actions: [],
            changed: false,
            initialized: true,
            saveFailed: false
        };
        var lockerCopy = {};
        initAll();
        return {
            modelNames: modelNames,
            status: status,
            data: data,
            init: init,
            save: save,
            initAll: initAll,
            saveAll: saveAll,
            locker: locker,
            importData: importData,
            exportData: exportData
        };


        function initAll() {
            for (var i = 0; i < modelNames.length; i++) {
                lockerCopy[modelNames[i]] = locker.get(modelNames[i], {});
            }
            for (var p in data) {
                if (data.hasOwnProperty(p))
                    delete data[p];
            }
            status.changed = false;
            status.actions.push('Storage init - Ok');
            angular.extend(data, lockerCopy);
        }

        function saveAll() {
            for (var i = 0; i < modelNames.length; i++) {
                try {
                    saveToLocker(modelNames[i], data[modelNames[i]]);
                    status.actions.push('Save ' + modelNames[i] + ' - Ok');
                } catch (err) {
                    status.actions.push('Save ' + modelNames[i] + ' - ' + err);
                }
            }

            status.changed = false;
        }

        function save(name) {
            try {
                saveToLocker(name, data[name]);
                status.actions.push('Save ' + name + ' - Ok');
            } catch (err) {
                status.actions.push('Save ' + name + ' - ' + err);
            }
        }

        function init(name) {
            locker.put(name, locker.get(name, {}));
            lockerCopy[name] = locker.get(name);
            for (var p in data[name]) {
                if (data[name].hasOwnProperty(p))
                    delete data[name][p];
            }
            angular.extend(data[name], lockerCopy[name]);
        }

        function saveToLocker(name, value) {
            if (angular.isObject(value)) {
                if (JSON.stringify(data).length > MAX_LOCAL_CAPACITY) {
                    status.saveFailed = true;
                    throw('max local storage exceeded - not saved');
                }
                locker.namespace(HP_PREVIOUS).put(name, locker.get(name));
                lockerCopy[name] = angular.copy(value);
                locker.put(name, value);
                if (!angular.equals(lockerCopy[name], locker.get(name))) {
                    status.saveFailed = true;
                    console.error('local', name, locker.get(name));
                    console.error('service', name, lockerCopy[name]);
                    throw(name + ' : service data and local storage data mismatch');
                }
                status.saveFailed = false;
            } else {
                throw('not an object');
            }
        }


        function importData() {
            function readSingleFile(e) {
                var file = e.target.files[0];
                if (!file) {
                    return;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    var contents = e.target.result;
                    displayContents(contents);
                };
                reader.readAsText(file);
            }

            function displayContents(contents) {
                var element = document.getElementById('file-content');
                element.innerHTML = contents;
            }
        }

        function exportData() {
            $window.open('data:application/octet-stream;base64,' + $window.btoa(JSON.stringify(data)))
        }


    }

    ])
    .directive('foreignKey', ['$filter', 'DataSource', 'Model', function ($filter, DS, Model) {
        return {
            // restrict to an attribute type.
            restrict: 'A',
            // element must have ng-model attribute.
            require: 'ngModel',
            link: function (scope, ele, attrs, ctrl) {
                // add a parser that will process each time the value is
                // parsed into the model when the user updates it.
                ctrl.$parsers.unshift(function (value) {
                    if (!value.trim().length) {
                        ctrl.$setValidity('invalidId' + attrs.foreignKey, false);
                        return ctrl.$modelValue;
                    }
                    var res = Model.processFk(value.trim(), attrs.foreignKey, angular.isDefined(attrs.many));
                    if (res)
                        DS.status.actions.push('Edit' + attrs.foreignKey + ' - Ok');
                    ctrl.$setValidity('invalidId' + attrs.foreignKey, res);
                    return res ? res : ctrl.$modelValue;
                });

                ctrl.$formatters.unshift(function (value) {
                    if (angular.isArray(value)) {
                        var res = '';
                        for (var i = 0; i < value.length; i++) {
                            var id = value[i];
                            res += id + ' ';
                        }
                        return res.trim();
                    } else {
                        return value.toString();
                    }
                });

            }
        };
    }])
    .directive('sequence', ['$filter', 'Utils', 'DataSource', function ($filter, Utils, DS) {
        return {
            // restrict to an attribute type.
            restrict: 'A',
            // element must have ng-model attribute.
            require: 'ngModel',
            link: function (scope, ele, attrs, ctrl) {
                // add a parser that will process each time the value is
                // parsed into the model when the user updates it.
                ctrl.$parsers.unshift(function (seq) {
                    seq = seq.trim();
                    var valid = true;
                    var ids = [];
                    var durations = [];
                    var tones = seq.split(' ');
                    for (var i = 0; i < tones.length; i++) {
                        if (tones[i].trim().length > 0) {
                            var tone = tones[i].split(',');
                            var id = tone[0].trim();
                            var duration = parseInt(tone[1]);
                            if (id && duration && id < 6 && id > 0) {
                                ids.push(id);
                                durations.push(duration);
                                continue;
                            }
                        }
                        valid = false;
                        DS.status.actions.push('Sequence update - ' + 'wrong note or duration');
                    }
                    if (durations.length !== ids.length)
                        valid = false;
                    ctrl.$setValidity('invalidSequence', valid);
                    if (valid)
                        DS.status.actions.push('Sequence update - ' + 'Ok');
                    return valid ? angular.extend({}, ctrl.$modelValue, {
                        keyIds: ids,
                        durations: durations,
                        length: ids.length
                    }) : ctrl.$modelValue;
                });
                ctrl.$formatters.unshift(function (value) {
                    var res = '';
                    if (angular.isArray(value.keyIds) && angular.isArray(value.durations) && value.id) {
                        for (var i = 0; i < value.keyIds.length; i++) {
                            var key = value.keyIds[i], duration = value.durations[i];
                            res += '' + key + ',' + duration;
                            if (i < value.keyIds.length - 1)
                                res += ' ';
                        }
                        return res;
                    } else {
                        DS.status.actions.push('sequence attributes missing' + JSON.stringify(value));
                    }
                });

            }
        };
    }]).factory('State', ['DataSource', function (DS) {
        var active = {};
        return {
            active: active
        }
    }])

;
