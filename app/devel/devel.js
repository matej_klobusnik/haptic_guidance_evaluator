'use strict';
angular.module('hapticPiano.devel', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/devel', {
            templateUrl: 'devel/devel.html',
            controller: 'DevelCtrl'
        });
    }])
    .controller('DevelCtrl', ['$scope', '$timeout', 'ArduinoPiano', 'State', 'locker', function ($scope, $timeout, ArduinoPiano, State, locker) {
        $scope.piano = ArduinoPiano;
        $scope.active = State.active;
        if (angular.isUndefined($scope.active.recordings))
            $scope.active.recordings = [];
        $scope.input = {sequence: localStorage.getItem('input.sequence') ? localStorage.getItem('input.sequence') : ''};

        $scope.$watch('input', function (n, o) {
            if (n !== o) {
                if (n.sequence && n.sequence !== o.sequence) {
                    localStorage.setItem('input.sequence', n.sequence);
                }
            }
        }, true);


        $scope.$watch('piano.audio', function (newValue, oldValue) {

        });


        $scope.$watch('active.recordings', function (n, o) {
            //if (n)
            //    $scope.piano.playOneNoteSequence(n);

            if (n) {
                var maxScale = 4000;
                var colors = ['red', 'blue', 'green', 'orange', 'grey'];
                for (var i = 0; i < n.length; i++) {
                    var start = n[i][0].start;
                    var end = n[i][n[i].length - 1].end;
                    var scale = $scope.contRec.trialDuration;
                    //var scale = end - start;
                    //if (scale > maxScale)
                    //    maxScale = scale > maxScale ? scale : maxScale;
                    for (var j = 0; j < n[i].length; j++) {
                        var note = n[i][j];
                        var nextNote = n[i][j+1];
                        var posStart = (note.start - start);
                        var width = Math.min(note.end - note.start, scale - posStart);

                        note.gap= nextNote ? nextNote.start - note.end :0;
                        note.nextSame= nextNote ? nextNote.keyId ===note.keyId :false;
                        note.nextMerge= note.nextSame && note.gap <100;

                        note.style = {
                            width: Math.floor(1000 * width / scale) / 10 + '%',
                            left: Math.floor(1000 * posStart / scale) / 10 + '%',
                            position: 'relative',
                            'background-color': colors[note.keyId - 1],
                            top: '-' + (18 * j) + 'px'
                        };
                    }
                }
            }

        }, true);


        $scope.$watch('piano.keyHist', function (n, o) {
            var keyHist = locker.get('keyHist', []);
            if (n === o) {
                keyHist.push({start: new Date().getTime(), date: new Date().toISOString(), data: []});
                locker.put('keyHist', keyHist);
            } else {
                $timeout(function () {
                    if (angular.isUndefined(keyHist[keyHist.length - 1]))
                        keyHist.push({start: new Date().getTime(), date: new Date().toISOString(), data: []});
                    var currentRecording = keyHist[keyHist.length - 1];
                    currentRecording.data = n;
                    locker.put('keyHist', keyHist);
                });
            }
        }, true);
        $scope.testSequences = [''];
        $scope.contRec = {
            trialDuration: 4000, status: 'stop',
            run: function () {
                $scope.contRec.status = 'recording';
                $scope.piano.startRecording();
                $timeout(function () {
                    $scope.piano.stopRecording();
                    if ($scope.contRec.status !== 'stop') {
                        $scope.contRec.status = 'pause';
                        $timeout(function () {
                            $scope.contRec.run();
                        }, 2000);
                    } else {
                        delete $scope.recording.start;
                    }
                }, $scope.contRec.trialDuration);
            }
        };
        $scope.devel = {
            testSeq: ['1,250 2,250 3,250 4,250 5,250', '1,300 2,300 3,300 4,300 5,300', '1,400 2,400 3,400 4,400 5,400'],
            trialDur: 4,
            results:[],
            run: function (seq) {
                results.push($scope.piano.playOneNoteSequence(seq));
            }
        };

        //$scope.chart = new JSUTILS.SignalScope("scope1", 900, 300, 0, 1);
        //$scope.chart.addMarker(0.81, '#00FF00');
        //$scope.chart.addMarker(0.99, '#00FF00');
        //
        //function animate() {
        //    $scope.chart.update($scope.piano.audio, $scope.piano.audioAverage);
        //    requestAnimFrame(animate);
        //}
        //animate();
    }])
;